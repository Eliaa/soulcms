<?php

/* ENABLE OR DISABLE MODULES
	|| # 	1 = ENABLE 		||
	|| # 	2 = DISABLE 	||
*/

/* Module User Panel */
$config['module_ucp'] = "1";

/* Module Top Players PVP */
$config['module_topPlayersPVP'] = "1";

/* Module Video Home Right */
$config['module_video1'] = "1";

/* Module Register */
$config['module_slides'] = "1";

/* Module News */
$config['module_news'] = "1";

/* Module Register */
$config['module_register'] = "1";

/* Module Login */
$config['module_login'] = "1";

/* Module Vote */
$config['module_vote'] = "1";

/* Module Donate */
$config['module_donate'] = "1";

/* Module Menu */
$config['module_menu'] = "1";