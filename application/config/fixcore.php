<?php 

/* SERVER NAME */
$config['projectname'] = "SoulCMS"; //replace

/* WOW VERSION || projectexpansion
	# ID: 8 == CUSTOM
	# ID: 7 == LEGION
	# ID: 6 == WARLORDS OF DRAENOR
	# ID: 5 == MIST OF PANDARIA
	# ID: 4 == CATACLYSM
	# ID: 3 == WRATH OF THE LICH KING
	# ID: 2 == THE BURNING CRUSADE
	# ID: 1 == VANILLA
*/
$config['projectexpansion'] = "7"; // ID replace

/* SERVER REALMLIST */
$config['projectRealmlist'] = "Set Realmlist fixcore.com"; //replace

/* Video */
$config['right_video'] = "https://player.twitch.tv/?channel=nexusgodx"; //replace

/* REALM */
$config['realm_name'] = "REALM NAME"; //replace
$config['realm_rates'] = "x1"; //replace
$config['realm_version'] = "3.3.5a"; //replace


/* SOCIAL LINKS */
$config['social_link_fb'] = ""; //replace
$config['social_link_tw'] = ""; //replace
$config['social_link_yt'] = ""; //replace
$config['social_link_it'] = ""; //replace