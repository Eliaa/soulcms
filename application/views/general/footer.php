<link rel='stylesheet' id='camera-css'  href='<?= base_url(); ?>assets/css/footer.css' type='text/css' media='all'> 
<footer class="footer-distributed">
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
				<a href=""><img src="<?= base_url(); ?>assets/images/dmca.png"></a>
			</div>

			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 color_white"></div>

			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 color_white">

				<p class="footer-company-about">
					<span>Follow us</span>
				</p>

				<div class="footer-icons">

					<a target="_blank" href="<?= $this->config->item('social_link_fb'); ?>"><i class="fa fa-facebook"></i></a>
					<a target="_blank" href="<?= $this->config->item('social_link_tw'); ?>"><i class="fa fa-twitter"></i></a>
					<a target="_blank" href="<?= $this->config->item('social_link_yt'); ?>"><i class="fa fa-youtube"></i></a>
					<a target="_blank" href="<?= $this->config->item('social_link_it'); ?>"><i class="fa fa-instagram"></i></a>

				</div>

			</div>

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 position_center">
				<p class="footer-links">
					<a href="#">Home</a>
					·
					<a href="#">Blog</a>
					·
					<a href="#">Faq</a>
					·
					<a href="#">Contact</a>
				</p>

				<p class="footer-company-name"><a class="color_white decoration" href="https://www.facebook.com/fixcore1/">FixCORE</a> &copy; 2015 - <?= date('Y'); ?> | All rights reserved</p>
			</div>
		</div>
</footer>

</body>
</html>
