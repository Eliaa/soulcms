<section class="container-fluid vidrio" style="height:30px;" id="head_bar">
      <div class="container">
        <div class="row">
          <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"><a href="<?= base_url(); ?>" class="fixcore_word decoration">FixCORE</a></div>
          <!-- left -->
          <?php if (isset($_SESSION['username'])) { ?>
            <?php if ($this->config->item('module_donate') == 1) { ?>
              <div class="col-md-1 col-xs-1"><a href="<?= base_url(); ?>donate" class="size_menu menu_fonts decoration space_menu">Donate</a></div>
            <?php } ?>
            <?php if ($this->config->item('module_vote') == 1) { ?>
              <div class="col-md-1 col-xs-1"><a href="<?= base_url(); ?>vote" class="size_menu menu_fonts decoration space_menu">Vote</a></div>
            <?php } ?>
          <?php } else { ?>
            <div class="cocol-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
          <?php } ?>
          <!-- right -->
            <!-- if is logout -->
            <?php if (isset($_SESSION['username'])) { ?>
              <div class="ccol-lg-9 col-md-9 col-sm-9 col-xs-9"><span class="pull-right"><a class="size_menu decoration menu_fonts" data-toggle="modal" href="#" data-target="#modalProfile">Welcome <?= $_SESSION['username']; ?> <i class="fa fa-chevron-down" aria-hidden="true"></i></a></span></div>
          <?php } else { ?>
            <!-- if is login -->
            <?php if ($this->config->item('module_register') == 1) { ?>
              <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 decoration"><span class="pull-right"><a class="size_menu decoration menu_fonts" href="<?= base_url(); ?>register">Register</a></span></div>
            <?php } ?>

            
            <?php if ($this->config->item('module_login') == 1) { ?>
              <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 decoration"><span class="pull-right"><a class="size_menu decoration menu_fonts" href="<?= base_url(); ?>login">Login</a></span></div>
            <?php } ?>
          <?php } ?>
        </div>
      </div>
</section>

<?php if ($this->config->item('module_menu') == 1) { ?>
<section id="menu" class="hidden-xs">
  <div class="container"></br>  
      <nav class="navbar navbar-inverse" data-spy="affix" data-offset-top="1">
        <ul class="nav navbar-nav">
        <?php foreach ($this->m_general->getMenu()->result() as $menus) { ?>
          <li><a class="menu_fonts1" href="<?= $menus->link ?>"><?= $menus->title ?></a></li>
        <?php } ?>
        </ul>
      </nav>
  </div>
</section>
<?php } ?>

<br class="visible-xs">


  <!-- Modal User -->
  <div class="modal fade" id="modalProfile" role="dialog">
    <div class="modal-dialog modal-sm">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title position_center"><?= $_SESSION['username']; ?></h4>
        </div>
        <div class="modal-body">
          <?php if ($this->config->item('module_ucp') == 1) { ?>
            <a href="<?= base_url(); ?>user"><ol class="breadcrumb"> <ul class="nav nav-pills"> <li role="presentation">User panel</li> </ul> </ol></a>
            <a href="<?= base_url(); ?>user/logout"><ol class="breadcrumb"> <ul class="nav nav-pills"> <li role="presentation" class="active">Logout</li> </ul> </ol></a>
          <?php } ?>
          <section id="points">
            <ol class="breadcrumb">
            <?php if ($this->config->item('module_donate') == 1) { $this->load->model('Donate/m_donate'); ?>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <img class="icon_vp_scale1" src="<?= base_url(); ?>assets/images/dp.png"> <a class="space_news decoration" href="<?= base_url(); ?>donate">DP: <?= $this->m_donate->getDp($_SESSION['userid']); ?></a>
              </div>
            <?php } ?>
            <?php if ($this->config->item('module_vote') == 1) { $this->load->model('Vote/m_vote'); ?>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"></br>
                <img class="icon_vp_scale1" src="<?= base_url(); ?>assets/images/vp.png"> <a class="space_news decoration" href="<?= base_url(); ?>vote">VP: <?= $this->m_vote->getVp($_SESSION['userid']); ?></a>
              </div>
            <?php } ?>
                </br></br>
              </div>
            </ol>
          </section>
        </div>
      </div>
      
    </div>
  </div>
