<!DOCTYPE html>
<html>
<head>
  <title><?= $this->config->item('projectname'); ?> - <?= $title ?></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- bootstrap -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <!-- menu -->
  <link rel="stylesheet" href="<?= base_url(); ?>assets/css/menu.css">
  <!-- general -->
  <link rel="stylesheet" href="<?= base_url(); ?>assets/css/general.css">
  <!-- fonts -->
  <link href="https://fonts.googleapis.com/css?family=Encode+Sans+Condensed" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Francois+One" rel="stylesheet">
  <!-- jquery -->
  <script type='text/javascript' src='<?= base_url(); ?>assets/scripts/jquery.min.js'></script>
  <script type='text/javascript' src='<?= base_url(); ?>assets/scripts/jquery.mobile.customized.min.js'></script>
  <!-- awesome -->
  <script src="https://use.fontawesome.com/8fbe83e7d7.js"></script>
  <!-- preloader -->
  <link rel="stylesheet" href="<?= base_url(); ?>assets/css/preloader.css">
  <script src="<?= base_url(); ?>assets/js/preloader.js"></script>
  <!-- modals -->
  <style>
  .affix {
      top: 0;
      width: 100%;
  }
  .affix + .container-fluid {
      padding-top: 70px;
  }
  </style>

</head>
<body>

<div id="load"></div>