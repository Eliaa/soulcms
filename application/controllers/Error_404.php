<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Error_404 extends MX_Controller {

	public function index()
	{
		redirect(base_url(),'refresh');
	}
}
