<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends MX_Controller {

	public function __construct()
    {
        parent::__construct();
    }

	public function index()
	{
		if (isset($_SESSION['username'])) {
			redirect(base_url(),'refresh');
		}
		else
		{
			if ($this->config->item('module_register') == 1) {
				$data['title'] = "Register";
			
				$this->load->model('m_register');

				$this->load->view('general/header', $data);
				$this->load->view('general/menu');
				$this->load->view('index');
				$this->load->view('general/footer');
			}
			else
				redirect(base_url(),'refresh');
		}
	}
}
