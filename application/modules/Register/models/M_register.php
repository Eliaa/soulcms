<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_register extends CI_Model {

    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
        if( ! ini_get('date.timezone') ) { date_default_timezone_set('GMT'); }
        $this->auth = $this->load->database('auth', TRUE);
    }

    public function insertRegister($username, $sha_pass, $email)
    {
        $typeRegister = $this->m_general->getTypeRegister($this->config->item('projectexpansion'));
        $passBattle = $this->m_general->encryptBattlenet($email, $sha_pass);
        $passAccount = $this->m_general->encryptAccount($username, $sha_pass);

        if ($this->auth->query("SELECT * FROM account WHERE email = '".$email."' OR username = '".$username."'")->num_rows() > 0) {
            echo '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"></div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"><div class="alert alert-danger">
                  <strong>Error!</strong> An account with this information already exists.</div></div><div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"></div>';
        }
        else
        {
            if ($typeRegister == 1) {
            $this->auth->query("INSERT INTO account (username, sha_pass_hash, email) VALUES ('$username', '$passAccount', '$email')");
            }
            else
            {
                
                $this->auth->query("INSERT INTO battlenet_accounts (email, sha_pass_hash) VALUES ('$email', '$passBattle')");

                $selectAccount = $this->auth->query("SELECT * FROM battlenet_accounts WHERE email = '".$email."'");
                $selectAccount = $selectAccount->row_array(); $selectAccount = $selectAccount['id'];

                $this->auth->query("INSERT INTO account (username, sha_pass_hash, email, battlenet_account, battlenet_index) VALUES ('$username', '$passAccount', '$email', '$selectAccount', '1')");

                echo '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"></div><div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"><div class="alert alert-success">
                          <strong>Success!</strong> Account created <a href="login" class="alert-link">Now you can login</a>. </div></div><div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"></div>';
            }
        }

    }

}