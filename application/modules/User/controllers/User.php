<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MX_Controller {

	public function __construct()
    {
        parent::__construct();
    }

    public function index()
	{
		if (isset($_SESSION['username'])) {
	
			if ($this->config->item('module_ucp') == 1) {
				$data['title'] = "User panel";

				$this->load->view('general/header', $data);
				$this->load->view('general/menu');
				$this->load->view('index');
				$this->load->view('general/footer');
			}
			else
				redirect(base_url(),'refresh');
		}
		else
			redirect(base_url(),'refresh');
	}

    public function logout()
    {
    	session_destroy();
    	redirect(base_url(),'refresh');
    }

}