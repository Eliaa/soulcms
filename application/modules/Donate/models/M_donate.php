<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_donate extends CI_Model {

    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
        if( ! ini_get('date.timezone') ) { date_default_timezone_set('GMT'); }
    }

    public function getDp($userid)
    {
        $query = $this->db->query("SELECT * FROM donate_points WHERE userid = '".$userid."'");
        
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $dp) {
                return $dp->dp;
            }
        }
        else
            return '0';
    }

}
