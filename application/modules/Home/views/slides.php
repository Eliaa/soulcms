<link rel='stylesheet' id='camera-css'  href='<?= base_url(); ?>assets/css/camera.css' type='text/css' media='all'> 
<script type='text/javascript' src='<?= base_url(); ?>assets/scripts/jquery.easing.1.3.js'></script> 
<script type='text/javascript' src='<?= base_url(); ?>assets/scripts/camera.min.js'></script> 

    <script>
		jQuery(function(){
			
			jQuery('#camera_wrap_1').camera({
				thumbnails: true
			});

			jQuery('#camera_wrap_2').camera({
				height: '400px',
				loader: 'bar',
				pagination: false,
				thumbnails: true
			});
		});
	</script>