<?php if ($this->config->item('module_slides') == 1) { ?>
<section id="slides" class="container">
    <div class="fluid_container">
        <div class="camera_wrap camera_azure_skin" id="camera_wrap_1">
    <?php foreach ($this->m_home->getSlides()->result() as $slides) { ?>
            <div data-thumb="<?= $slides->image ?>" data-src="<?= $slides->image ?>">
               <div class="camera_caption fadeFromBottom"> <a style="color: white;" class="decoration" href="<?= $slides->link ?>"><?= $slides->description ?></a> </div>
            </div>
    <?php } ?>
        </div>
    </div>
    
</section>
<?php } ?>

<section id="down" class="container">
    <?php if ($this->config->item('module_news') == 1) { ?>
    <div class="row">
      <div class="col-lg-1 col-md-12 col-sm-12 col-xs-12"></div>

      <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
        <div class="bg_top">
      </br>
            <div class="space_news">
            <?php foreach ($this->m_news->getNewsSpecify()->result() as $news) { ?>
                <div id="news">
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-4">
                        <img class="image_new_preview" src="<?= $news->image; ?>">
                    </div>
                    <div class="col-md6">
                        <h3 class="IndieFont color_white"><?= $news->title; ?></h3>
                        <p class="ArchitextsFont color_white"><?= substr($news->description, 0, 400).' ...'; ?></p>
                        <input class="btn btn-primary" type="submit" name="" value="See more">
                        <p class="spaceRight color_white position_right">Date: <?= date('Y/m/d', $news->created); ?></p>
                    </div>
                    </br>
                </div>
            <?php } ?>

            </div>
        </div>
      </div>
    <?php } ?>

      <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
        <div class="bg_right">
      </br>
            <div class="space_news">
            <?php if ($this->config->item('module_video1') == 1) { ?>
                <div class="hidden-xs">
                    <h3 class="color_white IndieFont"> <center>-- VIDEO --</center> </h3>
                    <center><iframe src="https://www.youtube.com/embed/Ka9esTQUgB8?controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe></center>
                    </br>
                </div>
            <?php } ?>
                <div>
                <hr>
                    <h3 class="color_white IndieFont"> <center>-- Donate -- </center> </h3>
                    <center>
                        <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                            <input type="hidden" name="cmd" value="_s-xclick">
                            <input type="hidden" name="hosted_button_id" value="5JBU55AT9BMLY">
                            <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                            <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                        </form>
                    </center>
                    </br>
                </div>
            <?php if ($this->config->item('module_topPlayersPVP') == 1) { ?>
                <div>
                    <hr>
                    <h3 class="color_white IndieFont"> <center>-- Top Players -- </center> </h3>

                    <!-- Default panel contents -->
                    <div class="panel-body color_white space_news">
                    <?php foreach ($this->m_top->getTopCharactersPVPSpecify()->result() as $topChar) { ?>
                      <p><img class="icon_class_top" src="<?= base_url(); ?>assets/images/class/<?= $this->m_characters->getIconClass($topChar->class); ?>"> <?= $topChar->name; ?></p>
                    <?php } ?>
                    
                    </div>

                </div>
            <?php } ?>
            </div>
        </div>
      </div>
    </div>
</section>