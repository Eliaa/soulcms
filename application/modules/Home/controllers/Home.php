<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MX_Controller {

	public function __construct()
    {
        parent::__construct();
    }

	public function index()
	{
		$data['title'] = "Home";
		
		$this->load->model('m_home');
		$this->load->model('News/m_news');
		$this->load->model('Top/m_top');
		$this->load->model('Characters/m_characters');

		$this->load->view('general/header', $data);
		$this->load->view('general/menu');
		$this->load->view('slides');
		$this->load->view('index');
		$this->load->view('general/footer');
	}
}
