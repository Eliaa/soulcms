<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_characters extends CI_Model {

    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
        if( ! ini_get('date.timezone') ) { date_default_timezone_set('GMT'); }
        $this->characters = $this->load->database('characters', TRUE);
    }

    public function getIconClass($class)
    {
        switch($class)
        {
            case 1: return 'warrior.png';
            case 2: return 'paladin.png';
            case 3: return 'hunter.png';
            case 4: return 'rogue.png';
            case 5: return 'priest.png';
            case 6: return 'dk.png';
            case 7: return 'shaman.png';
            case 8: return 'mage.png';
            case 9: return 'warlock.png';
            case 10: return 'monk.png';
            case 11: return 'druid.png';
            case 12: return 'dh.png';
        }
    }



}
