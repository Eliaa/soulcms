<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_news extends CI_Model {

    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
        if( ! ini_get('date.timezone') ) { date_default_timezone_set('GMT'); }
    }

    public function getNewsSpecify()
    {
        return $this->db->query("SELECT * FROM news WHERE active = 1 ORDER BY id DESC");
    }

}
