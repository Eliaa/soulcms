<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_login extends CI_Model {

    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
        if( ! ini_get('date.timezone') ) { date_default_timezone_set('GMT'); }
        $this->auth = $this->load->database('auth', TRUE);
    }

    public function getUserLogin1($username, $password)
    {
        $passAccount = $this->m_general->encryptAccount($username, $password);

        $selectAcc = $this->auth->query("SELECT * FROM account WHERE username = '".$username."' AND sha_pass_hash = '".$passAccount."'");

        if ($selectAcc->num_rows() > 0) {
            
            foreach ($selectAcc->result() as $sessionsAcc) {
                $_SESSION['username'] = $sessionsAcc->username;
                $_SESSION['userid'] = $sessionsAcc->id;
                $_SESSION['password_user'] = $sessionsAcc->sha_pass_hash;
                $_SESSION['email_user'] = $sessionsAcc->email;
                $_SESSION['joindate_user'] = $sessionsAcc->joindate;
                $_SESSION['expansion_user'] = $sessionsAcc->expansion;
            }

            redirect(base_url(),'refresh');

        }
        else
            echo '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"></div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"><div class="alert alert-danger">
            <strong>Error!</strong> Account not found.</div></div><div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"></div>';
    }

}