<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MX_Controller {

	public function __construct()
    {
        parent::__construct();
    }

	public function index()
	{
		if (isset($_SESSION['username'])) {
			redirect(base_url(),'refresh');
		}
		else
		{
			if ($this->config->item('module_login') == 1)
			{
				$data['title'] = "Login";

				$this->load->model('m_login');

				$this->load->view('general/header', $data);
				$this->load->view('general/menu');
				$this->load->view('index');
				$this->load->view('general/footer');
			}
			else
				redirect(base_url(),'refresh');
		}
	}
}
