<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_top extends CI_Model {

    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
        if( ! ini_get('date.timezone') ) { date_default_timezone_set('GMT'); }
        $this->characters = $this->load->database('characters', TRUE);
    }

    public function getTopCharactersPVPSpecify()
    {
        return $this->characters->query("SELECT * FROM characters ORDER BY totalKills DESC LIMIT 10");
    }


}
