<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_general extends CI_Model {

    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
        if( ! ini_get('date.timezone') ) { date_default_timezone_set('GMT'); }
        $this->auth = $this->load->database('auth', TRUE);
    }

    public function getExpansion($version)
    {
        switch($version)
        {
			case 1: return 'vanilla';
			case 2: return 'tbc';
			case 3: return 'wotlk';
			case 4: return 'cataclysm';
			case 5: return 'mop';
			case 6: return 'wod';
			case 7: return 'legion';
            case 8: return 'custom';
        }
        return false;
    }

    public function getTypeRegister($version)
    {
        switch($version)
        {
            case 1: return '1';
            case 2: return '1';
            case 3: return '1';
            case 4: return '1';
            case 5: return '1';
            case 6: return '2';
            case 7: return '2';
            case 8: return '2';
        }
        return false;
    }

    public function getExpansionColor($version)
    {
        switch($version)
        {
			case 1: return 'white';
			case 2: return 'green';
			case 3: return 'blue';
			case 4: return 'red';
			case 5: return 'yellow';
			case 6: return 'orange';
			case 7: return 'green';
            case 8: return 'purple';
        }
        return false;
    }

    public function encryptBattlenet($email, $password)
    {
        return strtoupper(bin2hex(strrev(hex2bin(strtoupper(hash("sha256",strtoupper(hash("sha256", strtoupper($email)).":".strtoupper($password))))))));
    }

    public function encryptAccount($username, $password)
    {
        if(!is_string($username)) { $username = ""; }
        if(!is_string($password)) { $password = ""; }
        return sha1(strtoupper($username).':'.strtoupper($password));
    }

    public function getMenu()
    {
        return $this->db->query("SELECT * FROM menu WHERE active = 1 ORDER BY id ASC");
    }


}