/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.7.14 : Database - soulcms
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`soulcms` /*!40100 DEFAULT CHARACTER SET latin1 */;

/*Table structure for table `donate_points` */

DROP TABLE IF EXISTS `donate_points`;

CREATE TABLE `donate_points` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `userid` int(10) NOT NULL,
  `dp` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `donate_points` */

insert  into `donate_points`(`id`,`userid`,`dp`) values (1,600,123);

/*Table structure for table `menu` */

DROP TABLE IF EXISTS `menu`;

CREATE TABLE `menu` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `link` text,
  `active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `menu` */

insert  into `menu`(`id`,`title`,`link`,`active`) values (1,'Home','home',1),(2,'Armory',NULL,1);

/*Table structure for table `news` */

DROP TABLE IF EXISTS `news`;

CREATE TABLE `news` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `created` int(10) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `news` */

insert  into `news`(`id`,`title`,`description`,`image`,`created`,`active`) values (1,'Welcome to SoulCMS','SOULCMS v0.0.0.1\r\n\r\nThanks for use','https://img10.deviantart.net/1c1f/i/2015/229/f/7/world_of_warcraft_screenshot_171_4k_by_imagebyjames-d964qim.jpg',1505029402,1),(2,'Help US','Help us with your donations to continue','https://img10.deviantart.net/1c1f/i/2015/229/f/7/world_of_warcraft_screenshot_171_4k_by_imagebyjames-d964qim.jpg',1505029444,1);

/*Table structure for table `slides` */

DROP TABLE IF EXISTS `slides`;

CREATE TABLE `slides` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `description` varchar(100) DEFAULT NULL,
  `image` text NOT NULL,
  `link` text,
  `active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `slides` */

insert  into `slides`(`id`,`description`,`image`,`link`,`active`) values (1,'Welcome to SoulCMS','https://img10.deviantart.net/1c1f/i/2015/229/f/7/world_of_warcraft_screenshot_171_4k_by_imagebyjames-d964qim.jpg',NULL,1),(2,'New Website','http://wallup.net/wp-content/uploads/2016/01/86022-Blood_Elf-Rogue-World_of_Warcraft.jpg',NULL,1),(3,'Help us to continue the project with your donations','http://nerdreactor.com/wp-content/uploads/2016/05/JSwvQWE.jpg','https://www.paypal.me/fixdevs',1);

/*Table structure for table `vote_points` */

DROP TABLE IF EXISTS `vote_points`;

CREATE TABLE `vote_points` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `userid` int(10) NOT NULL,
  `vp` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `vote_points` */

insert  into `vote_points`(`id`,`userid`,`vp`) values (1,600,22);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
